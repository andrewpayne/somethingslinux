+++
title = "ELILO with LUKS"
author = ["andrew"]
date = 2018-01-15
lastmod = 2018-12-12T17:02:28-08:00
tags = ["slackware"]
categories = ["general", "slackware", "encryption"]
draft = false
weight = -5
status = "report"
[menu.main]
  identifier = "elilo-with-luks"
  weight = -5
+++

## ELILO with LUKS {#elilo-with-luks}

LILO is the default bootloader, and it's [covered in slackdocs](https://docs.slackware.com/slackbook:booting) along with mkinitrd.  The UEFI/GPT variant called ELILO is configured in an almost identical way, except that:

-   typing `elilo` does not configure it automatically - you must edit the `elilo.conf` file by hand.

<!--listend-->

1.  Make an initrd.gz file:

    ```nil
    sh /usr/share/mkinitrd/mkinitrd_command_generator.sh -l /boot/vmlinuz-generic
    ```

    -   this will produce a long, ugly output... you need to re-write verbatim everything inside the single quotes `'` (e.g. `mkinitrd -c ... /boot/initrd.gz`) in your terminal prompt.

    -   hit `enter` and you'll build an intrd.gz on /boot as indicated

2.  I've found (at least with encrypted drives):
    -   the initrd.gz file needs to be placed here:

        ```nil
        /boot/efi/EFI/Slackware/initrd.gz
        ```
    -   the desired kernel should be copied to the same place - I usually cp vmlinuz-generic
    -   thus, the `elilo.conf` (also in this location) can be edited once and left alone even after kernel updates:

        ```nil
        image=vmlinuz-generic
        	initrd=initrd.gz
        	label=vmlinuz-generic
        	read-only
        	append="root=/dev/rootpartition vga=normal ro"
        ```

    -   I have had no problems with ELILO (old as it is) _except_ with NVIDIA drivers.  Using NVIDIA, after I launched an [X graphical environment](https://en.wikipedia.org/wiki/X%5FWindow%5FSystem) (i.e., my [window manager](https://wiki.archlinux.org/index.php/window%5Fmanager)), I was unable to return to a TTY, I had issues waking the device from sleep, and other problems.  Using [GRUB2](https://www.gnu.org/software/grub/grub-documentation.html) or [rEFInd](http://www.rodsbooks.com/refind/) solved all of them.  GRUB2 is included in the Slackware install iso.