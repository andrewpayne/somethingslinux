+++
title = "Flatpak for Slackware"
author = ["andrew"]
lastmod = 2019-06-04T12:01:49-07:00
tags = ["slackware", "flatpak"]
categories = ["Flatpak", "app-containers"]
draft = false
weight = -8
status = "report"
[menu.main]
  identifier = "flatpak-for-slackware"
  weight = -8
+++

## Flatpak for Slackware {#flatpak-for-slackware}

We're watching containers make the transition from revolution to standard.  In the app-container space, I've been happy with [Flatpak](<https://flatpak.org/>).  I know there were some eyebrows raised about security (I won't link to the most inflammatory material here, but google away if you dare...), but overall these issues appear to be (1) solvable, (2) comparable to those one finds with other solutions, and (3) not as severe as leaving a stack of $100 bills in your bike basket with a note that says "Property of Bicycle Owner."

After spending time with [ClearLinux](<https://clearlinux.org/>) (a great distro, I might add) which is <span class="underline">all containers, all the time</span>, I went from being an occassional Flatpak dabbler to having what I can only describe as a Flatpak Use Disorder (FUD).

I'm pleased that I can simply install apps that normally have sprawling dependency chains into self-contained sandboxes, and _remove_ the blooming things at a moments notice too.  I'm pleased that the containers are generally insensitive to my system's topography.  The app packs its own lunch, and isn't looking around my system for that special brand of mustard or else it won't run.

In short, with ClearLinux the well-known benefits of app containers were rejuvinated before my eyes.  But where is this leading?

Slackware.

Slackware has strengths, but one of them is not maintaining installs of things like LibreOffice, Skype, Steam, Nextcloud and so on.  Maintainers fall behind, and 14.2 (like any LTS-style release) is not the ideal stage for up-to-the-minute software.  But I use [Slackware's -current branch](<http://bear.alienbase.nl/mirrors/slackware/slackware64-current-iso/>), and that's a different ballgame.  The latest tools are all included.

So I took the aging 14.2 build of Flatpak and updated it to the most current version.  I also updated every dependency, and added all the new ones not found in the older slackbuilds.  Then I posted the whole tree in my [Github repo](<https://bit.ly/2EMB36v>).

The result is that I can run Slackware in all it's glory, but easily install and enjoy software that was not fun to build or maintain using Slackware's native tools.  Yes the Flatpak build itself is a little hairy, but that's _all_ I'm maintaining now.  Latest LibreOffice? [click]... done.  Latest Rhythmbox [click]... done.  Nexcloud [click]... done.  This is convenience folks.

You'll find a full set of installation tips on my Flatpak github page.  Have fun.