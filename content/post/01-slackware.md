+++
title = "Slackware"
author = ["andrew"]
date = 2017-10-26
lastmod = 2018-12-12T10:42:58-08:00
tags = ["slackware"]
categories = ["general"]
draft = false
weight = -1
status = "report"
[menu.main]
  identifier = "slackware"
  weight = -1
+++

## Slackware {#slackware}

I've driven a lot of [distros](https://distrowatch.com/) (my fellow distro hoppers will understand this) but my favorite remains [Slackware](http://www.slackware.com/).  There is a mountain of opinion around the web for and against, so I'll save you about a year of your life weighing the pros and cons and simply quote Dr. Zeuss: "You don't like it, so you say?  Try it!  Try it, and you may!"

1.  [slackdocs](https://docs.slackware.com/) is simply invaluable and and there's no subtitute for reading it.

2.  [Slackware\_Current](https://mirrors.slackware.com/slackware/) is the development branch, but I find it's plenty stable for daily driver use, and it gets regular updates and the latest kernels.  There is an iso of Current available [here](http://bear.alienbase.nl/mirrors/slackware/slackware64-current-iso/) (the slackware mirrors only have up to 14.2 isos).

3.  [14.2](https://mirrors.slackware.com/slackware/) is the latest LTS release.  I run it on my desktop, and I'm very happy with it.  It was fussy about my laptops since the older kernel (4.4) did not recognize certain hardware.

4.  [slackbuilds](https://slackbuilds.org) is the place to find third party software for 14.2.  There's also a [github repo](https://github.com/Ponce/slackbuilds) with software that's being tested with Current and thus may have been updated more recently.