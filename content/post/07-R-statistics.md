+++
title = "R Statistics for Slackware"
author = ["andrew"]
lastmod = 2019-03-26T15:24:23-07:00
tags = ["slackware", "data-science", "R"]
categories = ["Data-Science"]
draft = false
weight = -7
status = "report"
[menu.main]
  identifier = "r-statistics-for-slackware"
  weight = -7
+++

## R Statistics with Slackware {#r-statistics-with-slackware}

Perhaps motivated by another Slacker who [used and cited](https://slackalaxy.com/2019/03/15/slackware-and-slackbuilds-org-cited-in-a-scientific-publication/) Slackware in his recent publication, I decided to take on maintaining the R statistical package for Slackware.  To be honest, I have yet to find the definitve Linux distro for data science, and I'd hazard a guess that Windows probably ranks in there too.  I don't expect the data science community to drop everything and flock to Slackware to dance in the jubilent celebration of a freshly [updated R Slackbuild](https://slackbuilds.org/repository/14.2/academic/R/), but a few of you might and then I won't have to do it alone.  [R-studio](https://slackbuilds.org/repository/14.2/development/rstudio-desktop/?search=Rstudio) is available too from another maintainer.