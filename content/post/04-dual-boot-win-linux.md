+++
title = "Dual Boot Windows / Linux + LVM + LUKS"
author = ["andrew"]
date = 2017-12-15
lastmod = 2018-12-12T17:02:18-08:00
tags = ["slackware", "dual-boot", "multi-boot"]
categories = ["general"]
draft = false
weight = -4
status = "report"
[menu.main]
  identifier = "dual-boot-windows-linux-lvm-luks"
  weight = -4
+++

## Dual Boot Windows / Linux + LVM + LUKS {#dual-boot-windows-linux-lvm-luks}

I'll be using [Slackware](http://www.slackware.com/) in this example, but this layout is common in Linux and I've used it for [Arch](https://www.archlinux.org/), [Alpine](https://alpinelinux.org/), [Fedora](https://getfedora.org/), [Centos](https://www.centos.org/), [Void](https://voidlinux.org/) and others.


### Preparation: {#preparation}

It's handy to have one USB stick for each OS or distro you want to install.  They're cheap as dirt and most ISOs will fit on 8GB, and many a great deal less.

-   you can make one by downloading a slackware iso ([Current](http://bear.alienbase.nl/mirrors/slackware/slackware64-current-iso/) or [14.2](https://mirrors.slackware.com/slackware/slackware-iso/)) or whatever distro you're going to install.
-   `sudo dd if=/path/to/iso/file of=/dev/sdx status=progress`
    -   (sdx is the usb stick --> you'll want to make sure you get this part right)
-   **Windows users** should make your bootable windows iso on a windows box.  You can make a bootable slackware iso using [rufus](https://rufus.ie/en%5FIE.html) at the same time.


### Partitioning {#partitioning}

-   I find it easier in Linux, so I'll use my Slackware installer stick to boot up.
-   **Windows 10** - in my experience, Windows 10 is activated by a unique hardware key present on your device.  Thus wiping a drive with W10 pre-installed _shouldn't_ affect your license.  It's up to you to confirm.

<!--listend-->

1.  Boot Slackware USB

2.  Use `cfdisk` or `gdisk` to partition the drive with **3 partitions:**

    -   make one 600MiB type Linux
        -   _NOTE - some distros like Centos and Fedora want an efi partition and a boot partition.  In this case I make two Linux paritions of 256MiB and 600MiB_
    -   make one big partition of type Linux (about half the drive, to your preference)
    -   make one big partition of type Windows (yup, the other half)
    -   write to disk and exit

    ```nil
    mkfs.ext4 [ALL (2 or 3) Linux partitions]
    ```

    -   _NOTE - We're just doing this to trick windows into ignoring the partitions we will use for Linux_

    ```nil
    mkfs.fat -F 32 [the Windows partition]
    ```

3.  Reboot using Windows USB and install windows.  It should only see the FAT32 partition, and will reformat it NTFS etc.  It will NOT infiltrate the ext4 partitions.

4.  Reboot Slackware USB

5.  Use `cfdisk` or `gdisk` to:

    -   set the 600MiB partition to EFI (type ef00)

    <!--listend-->

    -   _NOTE - some distros like Centos and Fedora want an efi partition and a boot partition.  In this case set the 256MiB to efi (ef00) and the 600MiB to Linux (8300)_

6.  Format the EFI partition, e.g. /dev/sda1

    ```nil
    mkfs.fat -F 32 /dev/sda1
    ```

7.  Now let's say the big Linux partition is /dev/sda2
    -   _NOTE - of course just replace "xxx\_name" placeholders with your own_
    -   _swap size is your discretion_
    -   _the slackware installer will default to the first efi it finds_

        -   also see below or [tricking the installer](https://andrewpayne.gitlab.io/somethingslinux/post/post%5F0002/) to avoid putting slackware boot files on your windows partition

        ```nil
        cryptsetup luksFormat /dev/sda2
        cryptsetup luksOpen /dev/sda2 crypt_name

        pvcreate /dev/mapper/crypt_name

        vgcreate vg_name /dev/mapper/crypt_name

        lvcreate vg_name -L 8GiB vg_name -n swap
        lvcreate vg_name -l 50%FREE vg_name -n slackroot_name
        lvcreate vg_name -l 50%FREE vg_name -n otherroot_name

        mkswap /dev/vg_name/swap
        swapon /dev/vg_name/swap

        mkfs.ext4 /dev/vg_name/slackroot_name
        mount /dev/vg_name/sysroot /mnt

        mkdir /mnt/boot
        mount /dev/sda1 /mnt/boot

        ```

8.  Type `setup` to get going with your Slackware install and select the partitions in the installer

9.  The slackware installer `setup` is self-explanatory, _except_ for multiple boot systems.  It looks for an EFI boot partition, formatted FAT 32, and will use the first one it finds.  This is a problem if you have another Linux or Windows install already there.  I don't like to share /boot partitions between distros and certainly not with Windows.  But there's a workaround:

    ```nil
    mount your / partition to /mnt
    mkdir /mnt/boot/efi
    mount your desired /boot partition to /mnt/boot/efi
    ```

    -   start the installer `setup` and go through the first steps including selecting the partitions
    -   BEFORE installing the software, return to the terminal `[ctl]-[alt]-[F2]` and see if the slack installer has mounted a /boot/efi partition it shouldn't have
    -   unmount `umount` the **incorrect** /boot/efi partition but leave the correct one mounted
    -   return to the installer `[ctl]-[alt]-[F7]` and proceed with install - it will use the mounted /boot/efi partition

10. You'll be given a choice to install `LILO` (not used on EFI systems) or `ELILO`
    -   I prefer `GRUB` -- if you go this route skip ELILO
    -   otherwise install it and proceed

11. Either way, once the install is done, `[esc]` from the installer but **do not restart!**
    -   **correct /etc/fstab so the /boot/efi partition points the one you wanted (sdx#)**
    -   [ELILO configuration tips are here](https://andrewpayne.gitlab.io/somethingslinux/post/post%5F0005/)
    -   [GRUB configuration tips are here](https://andrewpayne.gitlab.io/somethingslinux/post/post%5F0006/)