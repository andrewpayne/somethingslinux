+++
title = "Slackware Multiboot: Tricking the installer"
author = ["andrew"]
date = 2017-11-01
lastmod = 2018-12-12T14:26:24-08:00
tags = ["slackware", "multiboot"]
categories = ["general"]
draft = false
weight = -2
status = "report"
[menu.main]
  identifier = "slackware-multiboot-tricking-the-installer"
  weight = -2
+++

## Slackware Multiboot: Tricking the installer {#slackware-multiboot-tricking-the-installer}

-   The slackware installer `setup` is self-explanatory, _except_ for multiple boot systems.  It looks for an EFI boot partition, formatted FAT 32, and will use the **first one it finds.**  This is a problem if you _already_ have another Linux or Windows installed.  I don't like to share /boot partitions between distros and certainly not with Windows.  But there's a workaround:

    ```nil
    boot your Slackware iso usb
    mount your / partition to /mnt
    mkdir /mnt/boot/efi
    mount your desired /boot partition to /mnt/boot/efi
    ```

-   start the installer `setup` and go through the first steps including selecting the partitions
-   **BEFORE** installing the packages, return to the terminal `[ctl]-[alt]-[F2]` and see if the slack installer has mounted a `/boot/efi` partition it shouldn't have
-   unmount `umount` the **incorrect** `/boot/efi` partition but leave the correct one mounted
-   return to the installer `[ctl]-[alt]-[F7]` and proceed with install - it will use the mounted `/boot/efi` partition
-   after installation, correct `/etc/fstab` so the `/boot/efi` entry (e.g. sda#) points the one you wanted