+++
title = "GRUB2 with LUKS"
author = ["andrew"]
date = 2021-03-14
lastmod = 2021-03-14T11:16:38-04:00
tags = ["slackware", "encryption", "grub"]
categories = ["general"]
weight = -6
draft = false
status = "report"
[menu.main]
  identifier = "grub2-with-luks"
  weight = -6
+++

## GRUB2 with LUKS {#grub2-with-luks}

It's a very common setup and poorly documented on the web.

I'll be using [Slackware](http://www.slackware.com/) in this example, but GRUB is used in many, many distros.

-   UEFI replaced BIOS years ago.  People still call everything BIOS, but if it was made lately, your computer is probably UEFI.
-   GPT replaced MBR years ago as the default partition scheme.  MBR refuses to die, but again, most recent systems are GPT.
-   I'm saying GRUB2 to be specific, but the original GRUB is rarely seen on new systems, so 'GRUB' and 'GRUB2' mean the same thing here.


### Preparation: {#preparation}

At this point, you've installed Slackware, hit `[esc]`, and you're looking at a terminal prompt.
I'm using the editor VI here because it's included on the Slackware iso

-   since it's not universally known or loved, I'll give VI commands for newbs

I'm assuming your new Slackware install is mounted at `/mnt` and the efi partition is at `/mnt/boot/efi`


### Steps {#steps}

1.  First we'll chroot into the new install:

    ```nil
    mount -t proc /proc /mnt/proc
    mount --rbind /dev /mnt/dev
    mount --rbind /sys /mnt/sys
    mount --rbind /run /mnt/run
    chroot /mnt
    ```

2.  Next we'll make an initrd.gz

    ```nil
    sh /usr/share/mkinitrd/mkinitrd_command_generator.sh -l /boot/vmlinuz-generic
    ```

    -   this will produce a long, ugly output... you need to re-write verbatim everything inside the single quotes `'` (e.g. `mkinitrd -c ... /boot/initrd.gz`) in your terminal prompt.

    -   hit `enter` and you'll build an intrd.gz on /boot as indicated

3.  Next we'll edit the grub config file:

    ```nil
    vi /etc/default/grub

        --> press i to enter text

        Add this as a final line for GRUB2.0:

    GRUB_CRYPTODISK_ENABLE=y

        In GRUB2.02 the syntax is reversed:

    GRUB_ENABLE_CRYPTODISK=y

        How to know which is which?  If you pick wrong, you'll just get an error in Step 5. Come back to this step and fix it.

        --> press [esc] to exit text entry mode
        --> :w to save and :q to quit
    ```

4.  Next we'll commit GRUB's config:

    ```nil
    mkdir -p /boot/grub
    grub-mkconfig -o /boot/grub/grub.cfg
    ```

5.  Finally, we'll install GRUB:

    ```nil
    grub-install --target=x86_64-efi --efi-directory=/boot/efi --bootloader-id="Slackware" --recheck
    ```

    -   you can name the bootloader-id anything you want

6.  No errors?  Reboot!
    -   You'll need to enter your LUKS password twice during boot.