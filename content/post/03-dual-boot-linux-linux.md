+++
title = "Dual Boot Linux / Linux + LVM + LUKS"
author = ["andrew"]
date = 2017-12-12
lastmod = 2018-12-12T17:02:22-08:00
tags = ["slackware", "dual-boot", "multi-boot"]
categories = ["general"]
draft = false
weight = -3
status = "report"
[menu.main]
  identifier = "dual-boot-linux-linux-lvm-luks"
  weight = -3
+++

## Dual Boot Linux / Linux + LVM + LUKS {#dual-boot-linux-linux-lvm-luks}

I'll be using [Slackware](http://www.slackware.com/) in this example, but this layout is common in Linux and I've used it for [Arch](https://www.archlinux.org/), [Alpine](https://alpinelinux.org/), [Fedora](https://getfedora.org/), [Centos](https://www.centos.org/), [Void](https://voidlinux.org/) and others.


### Preparation: {#preparation}

It's handy to have one USB stick for each OS or distro you want to install.  They're cheap as dirt and most ISOs will fit on 8GB, and many a great deal less.

-   you can make one by downloading a slackware iso ([Current](http://bear.alienbase.nl/mirrors/slackware/slackware64-current-iso/) or [14.2](https://mirrors.slackware.com/slackware/slackware-iso/)) or whatever distro you're going to install.
-   `sudo dd if=/path/to/iso/file of=/dev/sdx status=progress`
    -   (sdx is the usb stick --> **make sure you get this part right before you blow away your data**)


### Partitioning {#partitioning}

-   We'll install Slackware first.  If not, [see here](https://andrewpayne.gitlab.io/somethingslinux/post/post%5F0002/) for a way to get slackware to use the right boot drive.

Dual-boot Linux / Linux:

1.  Boot Slackware USB

2.  Use `cfdisk` or `gdisk` to partition the drive with

    -   2 efi (type ef00) partitions (I make them 600MiB)
    -   1 big Linux (type 8300) partition for the remaining space

    <!--listend-->

    -   _NOTE - some distros like Centos and Fedora want an efi partition **and** a boot partition.  In this case I make the efi (ef00) =256MiB and the boot (8300) =600MiB_

3.  Pick an EFI partition, e.g. /dev/sda1

    ```nil
    mkfs.fat -F 32 /dev/sda1
    ```

    Let's say the big partition is /dev/sda3

    -   _NOTE - of course just replace "xxx\_name" placeholders with your own_
    -   _swap size is your discretion_
    -   _the slackware installer will default to the first efi it finds_

        ```nil
        cryptsetup luksFormat /dev/sda3
        cryptsetup luksOpen /dev/sda3 crypt_name

        pvcreate /dev/mapper/crypt_name

        vgcreate vg_name /dev/mapper/crypt_name

        lvcreate vg_name -L 8GiB vg_name -n swap
        lvcreate vg_name -l 50%FREE vg_name -n slackroot_name
        lvcreate vg_name -l 50%FREE vg_name -n otherroot_name

        mkswap /dev/vg_name/swap
        swapon /dev/vg_name/swap

        mkfs.ext4 /dev/vg_name/slackroot_name
        mount /dev/vg_name/sysroot /mnt

        mkdir /mnt/boot
        mount /dev/sda1 /mnt/boot

        ```

4.  Type `setup` to get going with your install and select the partitions in the installer

5.  You'll be given a choice to install `LILO` (not used on EFI systems) or `ELILO`
    -   I prefer `GRUB` -- if you go this route skip ELILO
    -   otherwise install it and proceed

6.  Either way, once the install is done, `[esc]` from the installer but **do not restart!**
    -   [ELILO configuration tips are here](https://andrewpayne.gitlab.io/somethingslinux/post/post%5F0005/)
    -   [GRUB configuration tips are here](https://andrewpayne.gitlab.io/somethingslinux/post/post%5F0006/)