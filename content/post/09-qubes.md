+++
title = "Qubes"
author = ["andrew"]
lastmod = 2019-08-28T15:22:57-04:00
tags = ["qubes", "virtualization", "security"]
categories = ["qubes", "security"]
draft = false
weight = -9
status = "report"
[menu.main]
  identifier = "qubes"
  weight = -9
+++

## Qubes {#qubes}

So thanks to those clever people at the [chooselinux podcast](<https://chooselinux.show/>) I installed the distro called [Qubes OS](<https://www.qubes-os.org/>).  I've been using it, and folks: I like it.  I like it a lot.

Simply put, the goal here is security and as much of it as possible.  And also simplicity, with as much of that as possible too.  Qubes is essentially a framework for running a bunch of virtual machines.  If you're familiar with running them already, bad news...  you don't need such skills here.  Everything is configured, and the things that aren't configured are easy to manage through a simple Qubes interface.  All the business you normally have to fuss over with spinning up and destroying VMs is absent.  It's not that you can't get under the hood (it's a [Xen hypervisor](<https://xenproject.org/>) after all), but you don't have to.

I won't waste time explaining functionality since excellent [videos are here](<https://www.qubes-os.org/video-tours/>) and [documentation is here](<https://www.qubes-os.org/doc/>).  I'll focus on my experience.


### What problems are tackled? {#what-problems-are-tackled}

Ever find yourself in an airport or public area with dubious wifi sources?  I surely have, and with the ability to use isolated or disposable work environments is great, while still having a complete and functional system with my files available in protected but easily accessible VMs.

Ever get emails or attachments that just make you recoil in disgust at the likelihood of exposure to viral infection?  I do, and Qubes has me covered since I can open dodgy attachments in a disposable VM.  If it misbehaves, the VM and the viral payload are dumped like summer trash, leaving not the slightest stink on the rest of my system.

Ever want to install software that could be sketchy or could be cool?  Never has a new VM been easier to spin up and manage, including access to the files (and only the files) that you want for the project.

Ever want to compartmentalize sensitive material -- such as financial docs or labor intensive projects?  Again, the design is such that I can work on certain files in one VM, and click over to a web browser or email in another.  If I do something dumb (likely) or get pwned in my internet-facing VM, I can obliterate the damaged vessel and get right back to work without the slightest chance of contamination.


### So, is this a daily driver? {#so-is-this-a-daily-driver}

I'll say first, let's forget this idea.  I have a car and a bike and I ride the train too.  I'd buy a horse if I could afford to feed the thing.  There's no one distro that is the answer to all my needs, and since Linux is **free** and since [dual-boot](<https://andrewpayne.gitlab.io/somethingslinux/post/03-dual-boot-linux-linux/>) is **easy** and since hard drives are **massive** there need not be one answer.

-   I don't recommend Qubes for video editing or heavy processing tasks.  There are performance hits in VMs especially on laptops.
-   A Qubes system takes time to customize.  It took me some fiddling to get an organization scheme that is secure but convenient.
-   You can't just approach it like every other distro, or you'll kill the security.  It's a different way of thinking, and you have to get your head around it.

**Yeah, yeah, but is it a daily driver?**

Well, if your daily drive involves wanting to use your laptop in dubious environments while maintaining enough security to keep sensitive materials safe but accessible, and to have the luxury of clickety-clicking through webites and emails without the slightest care or caution?  Then yes, yes it is a daily driver.  Qubes is not a replacement for all other distros.  But when I need what it does, I've never seen anything do it better.